package me.redrield.scc;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TranslatableComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings("unused")
public class SimpleCommands implements CommandExecutor, Listener {


    private SimpleChatColour plugin;



    SimpleCommands(SimpleChatColour plugin)
    {
        this.plugin = plugin;
    }


    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args)
    {
        if(cs instanceof Player)
        {
            Player p = (Player) cs;
            if(args.length==1)
            {
                if(args[0].equalsIgnoreCase("help"))
                {
                    p.sendMessage(String.format("%s%s---------------%s%sChatColours+%s%s---------------", ChatColor.GOLD, ChatColor.STRIKETHROUGH, ChatColor.GREEN, ChatColor.BOLD, ChatColor.GOLD, ChatColor.STRIKETHROUGH));
                    TranslatableComponent version = new TranslatableComponent("Version " + plugin.pdf.getVersion());
                    version.setColor(ChatColor.RED);
                    p.spigot().sendMessage(version);
                    TranslatableComponent by = new TranslatableComponent("By Redrield");
                    by.setColor(ChatColor.RED);
                    p.spigot().sendMessage(by);
                    TranslatableComponent commands = new TranslatableComponent("Commands:");
                    commands.setColor(ChatColor.GOLD);
                    p.spigot().sendMessage(commands);
                    TranslatableComponent command = new TranslatableComponent("/scc Used to set chat colour \nCan also be used to assign someone's chat colour with /scc <player> <colour>");
                    command.setColor(ChatColor.BLUE);
                    p.spigot().sendMessage(command);
                    return true;
                }
            }
            if(args.length==0)
            {
                SCCInventory.openGUI(p);
                return true;
            }
            if(args.length==2)
            {
                if(!p.hasPermission("scc.colour.others")) {
                    TranslatableComponent noPerm = new TranslatableComponent("You don't have permission to set the colours of others!");
                    noPerm.setColor(ChatColor.RED);
                    p.spigot().sendMessage(noPerm);
                    return true;
                }
                OfflinePlayer target = Bukkit.getOfflinePlayer(UUIDFetcher.fetch(args[0]));

                if(target ==null)
                {
                    TranslatableComponent comp = new TranslatableComponent("That player doesn't exist!");
                    comp.setColor(ChatColor.RED);
                    p.spigot().sendMessage(comp);
                    return true;
                }
                if(target.getPlayer().hasPermission("scc.override"))
                {
                    TranslatableComponent comp = new TranslatableComponent("You can't change that player's chat colour!");
                    comp.setColor(ChatColor.RED);
                    p.spigot().sendMessage(comp);
                    return true;
                }
                try {
                    ChatColor.valueOf(args[1].toUpperCase());
                }catch(IllegalArgumentException e) {
                    TranslatableComponent comp = new TranslatableComponent("That colour doesn't exist!");
                    comp.setColor(ChatColor.RED);
                    p.spigot().sendMessage(comp);
                    return true;
                }
                final ChatColor cc = ChatColor.valueOf(args[1].toUpperCase());

                plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
                    try(Connection con = plugin.getHikari().getConnection(); PreparedStatement ps = con.prepareStatement(SQLOperations.UPSERT)) {
                        ps.setString(1, target.getUniqueId().toString());
                        ps.setString(2, cc.name());
                        ps.setString(3, cc.name());
                        plugin.getCache().remove(p);
                        ps.executeUpdate();
                    }catch(SQLException e) {
                        e.printStackTrace();
                    }
                });
                TranslatableComponent comp = new TranslatableComponent("That player's colour has been changed");
                comp.setColor(ChatColor.GREEN);
                p.spigot().sendMessage(comp);
            }
            return true;
        }
        return false;
    }

    @EventHandler
    public void onItemClick(InventoryClickEvent e)
    {
        if(e.getWhoClicked() instanceof Player)
        {
            Player p = (Player) e.getWhoClicked();
            if(ChatColor.stripColor(e.getInventory().getName()).equalsIgnoreCase("Chat Colours"))
            {
                e.setCancelled(true);
                switch(e.getCurrentItem().getType())
                {
                    case DIAMOND:
                        p.closeInventory();
                        SCCInventory.openEffectsGUI(p);
                        break;
                    case BLAZE_ROD:
                        Bukkit.dispatchCommand(p, "scc help");
                        p.closeInventory();
                        break;
                    case BEACON:
                        p.closeInventory();
                        SCCInventory.openColourGUI(p);
                        break;
                    case WOOL:
                        final ChatColor[] cc = new ChatColor[1];
                        for(ItemStack is : SCCInventory.getItemMap().keySet()) {
                            if(e.getCurrentItem().equals(is)) {
                                if(!p.hasPermission("scc.colour." + e.getCurrentItem().getType().name().toLowerCase()) && !p.hasPermission("scc.colour.*")) {
                                    TranslatableComponent comp = new TranslatableComponent("You don't have permission to use this colour!");
                                    comp.setColor(ChatColor.RED);
                                    p.spigot().sendMessage(comp);
                                    p.closeInventory();
                                    return;
                                }
                                cc[0] = SCCInventory.getItemMap().get(is);
                                break;
                            }
                        }
                        p.closeInventory();
                        if(e.getCurrentItem().equals(SCCInventory.getRedwool()))
                        {
                            Inventory whichred = Bukkit.createInventory(null, 9, ChatColor.RED + "Which red do you want?");
                            ItemStack is = new ItemStack(Material.REDSTONE_BLOCK);
                            ItemMeta im = is.getItemMeta();
                            im.setDisplayName(ChatColor.RED + "LIGHT RED");
                            is.setItemMeta(im);
                            ItemStack is2 = new ItemStack(Material.REDSTONE_BLOCK);
                            ItemMeta im2 = is2.getItemMeta();
                            im2.setDisplayName(ChatColor.DARK_RED + "DARK RED");
                            is2.setItemMeta(im2);
                            whichred.setItem(0, is);
                            whichred.setItem(8, is2);
                            p.closeInventory();
                            p.openInventory(whichred);
                        }
                        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
                            try(Connection con = plugin.getHikari().getConnection()) {
                                PreparedStatement ps = con.prepareStatement("UPDATE SimpleChatColour SET colour=? WHERE playerId=?");
                                ps.setString(1, cc[0].name());
                                ps.setString(2, p.getUniqueId().toString());
                                ps.executeUpdate();
                                ps.close();
                                con.close();
                            }catch(SQLException ex) {
                                ex.printStackTrace();
                            }
                        });
                        plugin.getCache().remove(p);
                        break;
                    case PAPER:
                        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
                            try(Connection con = plugin.getHikari().getConnection()) {
                                PreparedStatement select = con.prepareStatement(SQLOperations.SELECT);
                                select.setString(1, p.getUniqueId().toString());
                                ResultSet res = select.executeQuery();
                                if(res.next()) {
                                    if(res.getString("colour").equalsIgnoreCase("none") && res.getInt("bold")==0 && res.getInt("italics")==0 && res.getInt("magic") == 0) {
                                        TranslatableComponent comp = new TranslatableComponent("There aren't any colours to remove!");
                                        comp.setColor(ChatColor.RED);
                                        p.spigot().sendMessage(comp);
                                        return;
                                    }
                                    PreparedStatement ps = con.prepareStatement(SQLOperations.UPDATE);
                                    ps.setString(1, "none");
                                    ps.setInt(2, 0);
                                    ps.setInt(3, 0);
                                    ps.setInt(4, 0);
                                    ps.setString(5, p.getUniqueId().toString());
                                    ps.executeUpdate();
                                    ps.close();
                                    TranslatableComponent comp = new TranslatableComponent("Colour removed!");
                                    comp.setColor(ChatColor.GREEN);
                                    p.spigot().sendMessage(comp);
                                }
                                res.close();
                                select.close();
                            }catch(SQLException ex) {
                                ex.printStackTrace();
                            }
                        });
                        p.closeInventory();

                        plugin.getCache().remove(p);
                        break;
                    case EMERALD:
                        if(!p.hasPermission("scc.colour.rainbow")) {
                            TranslatableComponent noPerm = new TranslatableComponent("You don't have permission to use this colour!");
                            noPerm.setColor(ChatColor.RED);
                            p.spigot().sendMessage(noPerm);
                            p.closeInventory();
                            return;
                        }
                        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
                            try(Connection con = plugin.getHikari().getConnection()) {
                                PreparedStatement ps = con.prepareStatement("UPDATE SimpleChatColour SET colour=? WHERE playerId=?");
                                ps.setString(1, "RAINBOW");
                                ps.setString(2, p.getUniqueId().toString());
                                ps.executeUpdate();
                                ps.close();
                                con.close();
                            }catch(SQLException ex) {
                                ex.printStackTrace();
                            }
                        });
                        p.closeInventory();
                        plugin.getCache().remove(p);
                        break;
                }
            }else if(ChatColor.stripColor(e.getInventory().getName()).equalsIgnoreCase("Which red do you want?"))
            {
                e.setCancelled(true);
                switch(e.getCurrentItem().getType())
                {
                    case REDSTONE_BLOCK:
                        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
                            try(Connection con = plugin.getHikari().getConnection()) {
                                PreparedStatement ps;
                                if(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equals("LIGHT RED"))
                                {
                                    if(!p.hasPermission("scc.colour.light_red") && !p.hasPermission("scc.colour.*")) {
                                        TranslatableComponent comp = new TranslatableComponent("You don't have permission to use this colour!");
                                        comp.setColor(ChatColor.RED);
                                        p.spigot().sendMessage(comp);
                                        p.closeInventory();
                                        return;
                                    }
                                    ps = con.prepareStatement("UPDATE SimpleChatColour SET colour=? WHERE playerId=?");
                                    ps.setString(1, ChatColor.RED.name());
                                    ps.setString(2, p.getUniqueId().toString());
                                    ps.executeUpdate();
                                    ps.close();

                                }else if(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).equals("DARK RED"))
                                {
                                    if(!p.hasPermission("scc.colour.dark_red") && !p.hasPermission("scc.colour.*")) {
                                        TranslatableComponent comp = new TranslatableComponent("You don't have permission to ue this colour!");
                                        comp.setColor(ChatColor.RED);
                                        p.spigot().sendMessage(comp);
                                        p.closeInventory();
                                        return;
                                    }
                                    ps = con.prepareStatement("UPDATE SimpleChatColour SET colour=? WHERE playerId=?");
                                    ps.setString(1, ChatColor.DARK_RED.name());
                                    ps.setString(2, p.getUniqueId().toString());
                                    ps.executeUpdate();
                                    ps.close();
                                }
                            }catch(SQLException ex) {
                                ex.printStackTrace();
                            }
                        });
                        plugin.getCache().remove(p);
                        p.closeInventory();
                        break;
                }
                p.closeInventory();
            }else if(ChatColor.stripColor(e.getInventory().getName()).equalsIgnoreCase("Choose an effect!"))
            {
                e.setCancelled(true);
                plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
                    try(Connection con = plugin.getHikari().getConnection()) {
                        PreparedStatement select = con.prepareStatement(SQLOperations.SELECT);
                        select.setString(1, p.getUniqueId().toString());
                        ResultSet res = select.executeQuery();
                        int bold = 0;
                        int italics = 0;
                        int magic = 0;
                        if(res.next()) {
                            bold = res.getInt("bold");
                            italics = res.getInt("italics");
                            magic = res.getInt("magic");
                        }
                        res.close();
                        select.close();
                        PreparedStatement ps;
                        TranslatableComponent noPerm = new TranslatableComponent("You don't have permission to use this formatter!");
                        noPerm.setColor(ChatColor.RED);
                        switch(e.getCurrentItem().getType())
                        {
                            case DIAMOND_BLOCK:
                                if(!p.hasPermission("scc.colour.bold") && !p.hasPermission("scc.colour.*")) {
                                    p.spigot().sendMessage(noPerm);
                                    p.closeInventory();
                                    return;
                                }
                                ps = con.prepareStatement("UPDATE SimpleChatColour SET bold=? WHERE playerId=?");
                                ps.setInt(1, bold==0?1:0);
                                ps.setString(2, p.getUniqueId().toString());
                                ps.executeUpdate();
                                ps.close();
                                break;
                            case EMERALD_BLOCK:
                                if(!p.hasPermission("scc.colour.magic") && !p.hasPermission("scc.colour.*")) {
                                    p.spigot().sendMessage(noPerm);
                                    p.closeInventory();
                                    return;
                                }
                                ps = con.prepareStatement("UPDATE SimpleChatColour SET magic=? WHERE playerId=?");
                                ps.setInt(1, magic==0?1:0);
                                ps.setString(2, p.getUniqueId().toString());
                                ps.executeUpdate();
                                ps.close();
                                break;
                            case GOLD_BLOCK:
                                if(!p.hasPermission("scc.colour.italics") && !p.hasPermission("scc.colour.*")) {
                                    p.spigot().sendMessage(noPerm);
                                    p.closeInventory();
                                    return;
                                }
                                ps = con.prepareStatement("UPDATE SimpleChatColour SET italics=? WHERE playerId=?");
                                ps.setInt(1, italics==0?1:0);
                                ps.setString(2, p.getUniqueId().toString());
                                ps.executeUpdate();
                                ps.close();
                                break;
                        }
                    }catch(SQLException ex) {
                        ex.printStackTrace();
                    }
                });
                plugin.getCache().remove(p);
                p.closeInventory();
            }
        }
    }
}
