package me.redrield.scc;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.UUID;

public class UUIDFetcher {

    private static final String SESSION_SERVER = "https://sessionserver.mojang.com/session/minecraft/profile/";

    public static UUID fetch(String username) {
        UUID uuid = null;
        try {
            HttpsURLConnection con = (HttpsURLConnection) new URL(SESSION_SERVER + username).openConnection();
            JsonObject res = (JsonObject) new JsonParser().parse(new InputStreamReader(con.getInputStream()));
            uuid = UUID.fromString(res.get("id").getAsString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return uuid;
    }
}
