package me.redrield.scc;

public class SQLOperations {

    public static final String UPSERT = "INSERT INTO SimpleChatColour(playerId, colour) VALUES(?, ?) ON DUPLICATE KEY UPDATE colour=?";
    public static final String SELECT = "SELECT * FROM SimpleChatColour WHERE playerId=?";
    public static final String UPDATE = "UPDATE SimpleChatColour SET colour=?, bold=?, italics=?, magic=? WHERE playerId=?";
    public static final String INSERT = "INSERT INTO SimpleChatColour(playerId, playerName, colour, bold, italics, magic) VALUES(?, ?, ?, ?, ?, ?);";
}
