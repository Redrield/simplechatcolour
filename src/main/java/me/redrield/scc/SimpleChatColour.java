package me.redrield.scc;

import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class SimpleChatColour extends JavaPlugin{


    private HikariDataSource hikari;
    PluginDescriptionFile pdf = getDescription();
    org.bukkit.permissions.Permission chatColorPerm = new org.bukkit.permissions.Permission("scc");
    SimpleCommands sCmd = new SimpleCommands(this);
    private HashMap<Player, Cache> cache = new HashMap<>();


    @Override
    public void onEnable()
    {
        hikari = new HikariDataSource();
        this.getServer().getScheduler().runTaskAsynchronously(this, () -> {
            try {
                hikari.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
                hikari.addDataSourceProperty("serverName", getConfig().getString("mysql.ip"));
                hikari.addDataSourceProperty("port", getConfig().getString("mysql.port"));
                hikari.addDataSourceProperty("databaseName", getConfig().getString("mysql.database"));
                hikari.addDataSourceProperty("user", getConfig().getString("mysql.username"));
                hikari.addDataSourceProperty("password", getConfig().getString("mysql.password"));

                Connection con = hikari.getConnection();
                Statement statement = con.createStatement();
                statement.executeUpdate("CREATE TABLE IF NOT EXISTS SimpleChatColour(playerId CHAR(36) NOT NULL KEY, colour TEXT, bold TINYINT(1), italics TINYINT(1), magic TINYINT(1));");
                statement.close();
                con.close();
            }catch(NullPointerException | SQLException e) {
                getLogger().info("Unable to make connection to SQL database");
                getServer().getPluginManager().disablePlugin(this);
                return;
            }
        });


        saveDefaultConfig();

        chatColorPerm.getChildren().put("canset", true);
        chatColorPerm.getChildren().put("override", true);
        getServer().getPluginManager().addPermission(chatColorPerm);
        getCommand("scc").setExecutor(sCmd);
        getServer().getPluginManager().registerEvents(new SimpleListener(this), this);
        getServer().getPluginManager().registerEvents(new SimpleCommands(this), this);
        try {
            update();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        hikari = null;
    }

    public HikariDataSource getHikari() {
        return hikari;
    }

    public HashMap<Player, Cache> getCache() {
        return cache;
    }

    private void update() throws IOException {
        URL spigot = new URL("http://www.spigotmc.org/api/general.php");
        HttpURLConnection con = (HttpURLConnection) spigot.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.getOutputStream().write("key=98BE0FE67F88AB82B4C197FAF1DC3B69206EFDCC4D3B80FC83A00037510B99B4&resource=14653".getBytes());
        String ver = new BufferedReader(new InputStreamReader(con.getInputStream())).readLine();
        if(Integer.parseInt(ver.replace(".", ""))>Integer.parseInt(pdf.getVersion().replace(".", ""))) {
            File update = new File("plugins" + File.separator + "update");
            if(!update.exists()) {
                update.mkdir();
            }
            ReadableByteChannel in = Channels.newChannel(new URL("https://api.spiget.org/v1/resources/14653/download").openStream());
            FileChannel out = new FileOutputStream(new File(update, "ChatColours+.jar")).getChannel();
            out.transferFrom(in, 0, Long.MAX_VALUE);
            Bukkit.getConsoleSender().sendMessage(String.format("%s[" + pdf.getName() + "] New version downloaded! Your version: " + pdf.getVersion() + ". Newest version: " + ver + "\nRestart server for update to take effect", ChatColor.YELLOW));
        }
    }
}
