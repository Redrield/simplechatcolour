package me.redrield.scc;

import org.bukkit.Bukkit;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SCCInventory {
    //GUI Items
    private static HashMap<ItemStack, ChatColor> itemMap = new HashMap<>();
    private static ItemStack removeColors = new ItemStack(Material.PAPER);
    private static ItemStack seeColors = new ItemStack(Material.BEACON);
    private static ItemStack instructions = new ItemStack(Material.BLAZE_ROD);
    private static ItemStack whitewool = new ItemStack(Material.WOOL);
    private static ItemStack goldwool = new ItemStack(Material.WOOL, 1, (byte)1);
    private static ItemStack lightpurplewool = new ItemStack(Material.WOOL, 1, (byte)2);
    private static ItemStack aquawool = new ItemStack(Material.WOOL, 1, (byte)3);
    private static ItemStack yellowwool = new ItemStack(Material.WOOL, 1, (byte)4);
    private static ItemStack lightgreenwool = new ItemStack(Material.WOOL, 1, (byte)5);
    private static ItemStack darkgreywool = new ItemStack(Material.WOOL, 1, (byte)7);
    private static ItemStack darkaquawool = new ItemStack(Material.WOOL, 1, (byte)9);
    private static ItemStack purplewool = new ItemStack(Material.WOOL, 1, (byte)10);
    private static ItemStack darkbluewool = new ItemStack(Material.WOOL, 1, (byte)11);
    private static ItemStack darkgreenwool = new ItemStack(Material.WOOL, 1, (byte)13);
    private static ItemStack redwool = new ItemStack(Material.WOOL, 1, (byte)14);
    private static ItemMeta whitemeta = whitewool.getItemMeta();
    private static ItemMeta goldmeta = goldwool.getItemMeta();
    private static ItemMeta lpmeta = lightpurplewool.getItemMeta();
    private static ItemMeta aquameta = aquawool.getItemMeta();
    private static ItemMeta yellowmeta = yellowwool.getItemMeta();
    private static ItemMeta lgmeta = lightgreenwool.getItemMeta();
    private static ItemMeta dgmeta = darkgreywool.getItemMeta();
    private static ItemMeta dameta = darkaquawool.getItemMeta();
    private static ItemMeta purplemeta = purplewool.getItemMeta();
    private static ItemMeta dbmeta = darkbluewool.getItemMeta();
    private static ItemMeta dgreenmeta = darkgreenwool.getItemMeta();
    private static ItemMeta redmeta = redwool.getItemMeta();
    private static Inventory gui = Bukkit.createInventory(null, 54, ChatColor.DARK_GREEN + "Chat Colours");
    private static Inventory newgui = Bukkit.createInventory(null, 45, ChatColor.GREEN + "Chat Colours");
    private static Inventory fx = Bukkit.createInventory(null, 9, ChatColor.DARK_GREEN + "Choose an effect!");
    private static ItemStack bold = new ItemStack(Material.DIAMOND_BLOCK,1);
    private static ItemStack obfuscated = new ItemStack(Material.EMERALD_BLOCK,1);
    private static ItemStack italics = new ItemStack(Material.GOLD_BLOCK,1);
    private static ItemStack fxclick = new ItemStack(Material.DIAMOND,1);
    private static ItemStack rainbow = new ItemStack(Material.EMERALD, 1);
    private static ItemMeta rainbowMeta = rainbow.getItemMeta();

    //Creating the gui
    public static void openGUI(Player player)
    {
        ItemMeta remIM = removeColors.getItemMeta();
        List<String> lore = new ArrayList<String>();
        lore.add(String.format("%s%sRemoves all colours", ChatColor.RED, ChatColor.BOLD));
        remIM.setLore(lore);
        remIM.setDisplayName(String.format("%s%sReset your chat colour", ChatColor.DARK_RED, ChatColor.BOLD));
        removeColors.setItemMeta(remIM);
        gui.setItem(49, removeColors);
        ItemMeta seeIM = seeColors.getItemMeta();
        lore.remove(0);
        lore.add(String.format("%s%sSee all the colour options!", ChatColor.GREEN, ChatColor.ITALIC));
        seeIM.setLore(lore);
        seeIM.setDisplayName(ChatColor.GOLD + "Colours");
        seeColors.setItemMeta(seeIM);
        gui.setItem(13, seeColors);
        ItemMeta insIM = instructions.getItemMeta();
        lore.clear();
        lore.add(ChatColor.GOLD + "How to use! SEE THIS FIRST");
        insIM.setDisplayName(ChatColor.DARK_GREEN + "How to use!");
        insIM.setLore(lore);
        instructions.setItemMeta(insIM);
        lore.clear();
        ItemMeta fxcim = fxclick.getItemMeta();
        lore.add(ChatColor.DARK_BLUE + "Add some special effects to your chat!");
        fxcim.setLore(lore);
        lore.clear();
        fxcim.setDisplayName(ChatColor.DARK_GREEN + "Chat Effects");
        fxclick.setItemMeta(fxcim);
        gui.setItem(0, instructions);
        gui.setItem(1, instructions);
        gui.setItem(2, instructions);
        gui.setItem(3, instructions);
        gui.setItem(4, instructions);
        gui.setItem(5, instructions);
        gui.setItem(6, instructions);
        gui.setItem(7, instructions);
        gui.setItem(8, instructions);
        gui.setItem(31, fxclick);
        player.openInventory(gui);
    }

    public static void openEffectsGUI(Player p) {
        ItemMeta boldim = bold.getItemMeta();
        ItemMeta italim = italics.getItemMeta();
        ItemMeta obfim = obfuscated.getItemMeta();
        boldim.setDisplayName(String.format("%s%sBOLD",ChatColor.GOLD, ChatColor.BOLD));
        bold.setItemMeta(boldim);
        italim.setDisplayName(String.format("%s%sITALICS",ChatColor.GOLD, ChatColor.ITALIC));
        italics.setItemMeta(italim);
        obfim.setDisplayName(String.format("%s%sII%sMAGIC%s%sII",ChatColor.RED, ChatColor.MAGIC, ChatColor.GOLD, ChatColor.RED, ChatColor.MAGIC));
        obfuscated.setItemMeta(obfim);
        fx.setItem(1, bold);
        fx.setItem(4, obfuscated);
        fx.setItem(7, italics);
        p.openInventory(fx);
    }


    public static void openColourGUI(Player p) {
        whitemeta.setDisplayName(ChatColor.WHITE + "WHITE");
        whitewool.setItemMeta(whitemeta);
        newgui.setItem(1, whitewool);
        goldmeta.setDisplayName(ChatColor.GOLD + "GOLD");
        goldwool.setItemMeta(goldmeta);
        newgui.setItem(3, goldwool);
        lpmeta.setDisplayName(ChatColor.LIGHT_PURPLE + "LIGHT PURPLE");
        lightpurplewool.setItemMeta(lpmeta);
        newgui.setItem(5, lightpurplewool);
        aquameta.setDisplayName(ChatColor.AQUA + "AQUA");
        aquawool.setItemMeta(aquameta);
        newgui.setItem(7, aquawool);
        yellowmeta.setDisplayName(ChatColor.YELLOW + "YELLOW");
        yellowwool.setItemMeta(yellowmeta);
        newgui.setItem(19, yellowwool);
        lgmeta.setDisplayName(ChatColor.GREEN + "GREEN");
        lightgreenwool.setItemMeta(lgmeta);
        newgui.setItem(21, lightgreenwool);
        rainbowMeta.setDisplayName(String.format("%sR%sA%sI%sN%sB%sO%sW", ChatColor.RED, ChatColor.GOLD, ChatColor.YELLOW, ChatColor.GREEN, ChatColor.BLUE, ChatColor.DARK_BLUE, ChatColor.LIGHT_PURPLE));
        rainbow.setItemMeta(rainbowMeta);
        newgui.setItem(22, rainbow);
        dgmeta.setDisplayName(ChatColor.DARK_GRAY + "DARK GRAY");
        darkgreywool.setItemMeta(dgmeta);
        newgui.setItem(23, darkgreywool);
        dameta.setDisplayName(ChatColor.DARK_AQUA + "DARK AQUA");
        darkaquawool.setItemMeta(dameta);
        newgui.setItem(25, darkaquawool);
        purplemeta.setDisplayName(ChatColor.DARK_PURPLE + "DARK PURPLE");
        purplewool.setItemMeta(purplemeta);
        newgui.setItem(37, purplewool);
        dbmeta.setDisplayName(ChatColor.DARK_BLUE + "DARK BLUE");
        darkbluewool.setItemMeta(dbmeta);
        newgui.setItem(39, darkbluewool);
        dgreenmeta.setDisplayName(ChatColor.DARK_GREEN + "DARK GREEN");
        darkgreenwool.setItemMeta(dgreenmeta);
        newgui.setItem(41, darkgreenwool);
        redmeta.setDisplayName(ChatColor.RED + "RED");
        redwool.setItemMeta(redmeta);
        newgui.setItem(43, redwool);
        p.openInventory(newgui);
    }

    public static HashMap<ItemStack, ChatColor> getItemMap() {
        itemMap.put(whitewool, ChatColor.WHITE);
        itemMap.put(goldwool, ChatColor.GOLD);
        itemMap.put(lightpurplewool, ChatColor.LIGHT_PURPLE);
        itemMap.put(aquawool, ChatColor.AQUA);
        itemMap.put(yellowwool, ChatColor.YELLOW);
        itemMap.put(lightgreenwool, ChatColor.GREEN);
        itemMap.put(darkgreywool, ChatColor.DARK_GRAY);
        itemMap.put(darkaquawool, ChatColor.DARK_AQUA);
        itemMap.put(purplewool, ChatColor.DARK_PURPLE);
        itemMap.put(darkbluewool, ChatColor.DARK_BLUE);
        itemMap.put(darkgreenwool, ChatColor.DARK_GREEN);
        return itemMap;
    }

    public static ItemStack getRedwool() {
        return redwool;
    }
}
