package me.redrield.scc;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


@SuppressWarnings("unused")
public class SimpleListener implements Listener {

    private SimpleChatColour plugin;

    public SimpleListener(SimpleChatColour instance)
    {
        this.plugin = instance;
    }


    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent e)
    {
        Player p = e.getPlayer();
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
            try (Connection con = plugin.getHikari().getConnection(); PreparedStatement ps = con.prepareStatement("SELECT * FROM SimpleChatColour WHERE playerId=?")) {
                ps.setString(1, p.getUniqueId().toString());
                ResultSet res = ps.executeQuery();
                if(!res.next()) {
                    PreparedStatement insert = con.prepareStatement("INSERT INTO SimpleChatColour VALUES(?, ?, ?, ?, ?);");
                    insert.setString(1, p.getUniqueId().toString());
                    insert.setString(2, "none");
                    insert.setInt(3, 0);
                    insert.setInt(4, 0);
                    insert.setInt(5, 0);
                    insert.executeUpdate();
                    insert.close();
                }
                res.close();

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerChatEvent(AsyncPlayerChatEvent e)
    {
        Player p = e.getPlayer();
        if(plugin.getCache().containsKey(p)) {
            Cache c = plugin.getCache().get(p);
            TextComponent component = new TextComponent(e.getMessage());
            component.setColor(c.getChatColor());
            component.setBold(c.isBold());
            component.setItalic(c.isItalic());
            component.setObfuscated(c.isMagic());
            e.setMessage(component.toLegacyText());
        }else {
            try(Connection con = plugin.getHikari().getConnection(); PreparedStatement ps = con.prepareStatement(SQLOperations.SELECT)) {
                ps.setString(1, p.getUniqueId().toString());
                ResultSet res = ps.executeQuery();
                if(res.next()) {
                    if (res.getInt("bold") == 0 && res.getInt("italics") == 0 && res.getInt("magic") == 0 && res.getString("colour").equalsIgnoreCase("none")) {
                        plugin.getCache().put(p, Cache.cache(null, 0, 0, 0));
                        return;
                    }
                    if(res.getString("colour").equals("RAINBOW")) {
                        char[] mes = e.getMessage().toCharArray();

                        StringBuilder message = new StringBuilder();
                        int count = 0;
                        for(int i = 0; i < mes.length; i++) {
                            ChatColor[] colours = {ChatColor.RED, ChatColor.GOLD, ChatColor.YELLOW, ChatColor.GREEN, ChatColor.BLUE, ChatColor.DARK_BLUE, ChatColor.LIGHT_PURPLE};
                            if(count>6) {
                                count = 0;
                            }
                            message.append(colours[count] + String.valueOf(mes[i]));
                            count++;
                        }
                        e.setMessage(message.toString());
                        return;
                    }
                    ChatColor cc = null;
                    TextComponent component = new TextComponent(e.getMessage());
                    if (!res.getString("colour").equalsIgnoreCase("none")) {
                        cc = ChatColor.valueOf(res.getString("colour"));
                    }
                    int bold = res.getInt("bold");
                    int italics = res.getInt("italics");
                    int magic = res.getInt("magic");
                    if (cc != null) {
                        component.setColor(cc);
                    }
                    component.setBold(bold==1);
                    component.setItalic(italics==1);
                    component.setObfuscated(magic==1);
                    plugin.getCache().put(p, Cache.cache(cc, bold, italics, magic));
                    e.setMessage(component.toLegacyText());
                }
                res.close();
            }catch(SQLException ex) {
                ex.printStackTrace();
            }
        }

    }
}
