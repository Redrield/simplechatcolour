package me.redrield.scc;

import net.md_5.bungee.api.ChatColor;

public class Cache {

    private ChatColor cc;
    private boolean bold;
    private boolean italic;
    private boolean magic;

    public boolean isBold() {
        return bold;
    }

    public boolean isItalic() {
        return italic;
    }

    public boolean isMagic() {
        return magic;
    }

    public ChatColor getChatColor() {
        return cc;
    }

    private Cache(ChatColor cc, boolean bold, boolean italic, boolean magic) {
        this.cc = cc;
        this.bold = bold;
        this.italic = italic;
        this.magic = magic;
    }

    public static Cache cache(ChatColor colour, int bold, int italic, int magic) {
        return new Cache(colour, bold == 1, italic == 1, magic == 1);
    }
}
